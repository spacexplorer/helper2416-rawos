/*
* Copyright (c) 2013
* All rights reserved.
* 
* 文件名称：lcd_driver.C
* 文件标识：
* 开发环境：C编译器
* 描    述：touch panel driver
* 当前版本：V1.0
* 作    者：
* 更改说明：
* 完成日期：2013年04月01日
*
* 修改记录 
*       作者        时间        版本            修改描述
*     章光泉      20130402      V1.0        
*/
#include <stdio.h>
#include <lib_string.h>
#include <raw_api.h>
#include <irq.h>
#include "2416_reg.h"
#include "ADCTs_driver.h"
#include "GUI.h"

extern RAW_U32 get_PCLK(void);

#define BIT_ADC         0x80000000
#define BIT_SUB_TC      (0x1<<9)
#define BIT_SUB_ADC     (0x1<<10)


/****************************全局变量定义开始************************************/

RAW_U32 adcpsr=0;           // ADCPSR   
RAW_U32 adcclock=500000;   // ADC clock
RAW_U32 xtal=12000000;      // X-tal clock
  
volatile RAW_S32 xdata, ydata;
RAW_U32 count=0;            // check interrupt

/****************************全局变量定义结束************************************/

/****************************全局函数定义开始************************************/
void AdcTsAuto(void);
/****************************全局函数定义结束************************************/

/*************************************************
    函数名称:       adc_ts_init
    功能描述:       触摸屏初始化
    子 函 数:       
    调用限制:       
    输入参数:       无
    输出参数:       无
    返 回 值:       无
    说    明:       
*************************************************/
void adc_ts_init(void)
{
    //Uart_Printf("[ADC touch screen test.]\n");
    
    adcpsr=(RAW_U32)((get_PCLK()/adcclock)-1);
    //Uart_Printf("PSR=%d, ADC freq.=%dHz\n", adcpsr, (int)(get_PCLK()/(adcpsr+1)));
    
    ADCDLY_REG=(int)(xtal/1000)*2;  // 2ms delay(filter effect)
    //Uart_Printf("ADCDLY = %d\n", ADCCON_REG);
    
    ADCCON_REG = ADCCON_REG & ~(0xffff) | (1<<14) | (adcpsr<<6)/* |(1<<3)|(0<<2)*/;//manual start
    //Uart_Printf("ADCCON_REG = 0x%x\n", ADCCON_REG);
    
    ADCTSC_REG = 0xd3/*0xd7*/;  //Wfint,XP_PU_dis,XP_Dis,XM_Dis,YP_Dis,YM_En
        
    register_irq(31, AdcTsAuto);        // Auto measurement
    
    SUBSRCPND_REG |= (RAW_U32)BIT_SUB_ADC;// Clear previous pending bit
    INTMSK_REG &= ~BIT_ADC;       // ADC Touch Screen Mask bit clear
    INTSUBMSK_REG &=~(BIT_SUB_ADC | BIT_SUB_TC);
    
    //Uart_Printf("\nTouch panel initial over!... \n\n");
    //Uart_Printf("\nWating Stylus Down... \n\n");
#if 0
    INTSUBMSK_REG |= (BIT_SUB_ADC|BIT_SUB_TC);
    INTMSK_REG |= (RAW_U32)BIT_ADC;
    ADCCON_REG &= ~(1<<14);// disable prescaler
    ADCCON_REG |= (1<<2);   // Standby
#endif
}

/*************************************************
    函数名称:       AdcTsAuto
    功能描述:       AD & TS int
    子 函 数:       
    调用限制:       
    输入参数:       无
    输出参数:       无
    返 回 值:       无
    说    明:       
*************************************************/
void AdcTsAuto(void)
{
#if 0
    INTMSK_REG |= (RAW_U32)BIT_ADC;     //ADC Touch Screen Mask bit clear
    INTSUBMSK_REG |= (BIT_SUB_ADC | BIT_SUB_TC);
#endif
    //三星原厂代码
    if(SUBSRCPND_REG & (0x1<<10))       // ADC interrupt
    {
        xdata = (ADCDAT0_REG&0xfff);
        ydata = (ADCDAT1_REG&0xfff);
		
        //Uart_Printf("count=%d XP=%04d, YP=%04d\n", count++, xdata, ydata);

        ADCTSC_REG = 0x1d3;         // Up detect, Waiting for interrupt

        // 清INT_ADC中断
        SUBSRCPND_REG |= BIT_SUB_ADC;
        SRCPND_REG    |= BIT_ADC;
        INTPND_REG    |= BIT_ADC;
    }
    else if(SUBSRCPND_REG & (0x1<<9))   // TC interrupt
    {
        if(ADCDAT0_REG & 0x8000)    // Stylus Up
        {
            Uart_Printf("Stylus Up\n\n");            
            ADCTSC_REG = 0xd3;          // Down detect, Waiting for interrupt
            #if 0  //debug
            INTMSK_REG |= (RAW_U32)BIT_ADC;     //ADC Touch Screen Mask bit clear
            INTSUBMSK_REG |= (BIT_SUB_ADC | BIT_SUB_TC);
            #endif
        }
        else            // Stylus Down
        {
            Uart_Printf("Stylus Down\n");
            
            ADCDLY_REG=(get_PCLK()/1000000)*500;// 500us delay(input stable)
            ADCTSC_REG = 0x0c;;     /* 进入自动(连续) X/Y轴坐标转换模式 */
            ADCCON_REG |= 0x1;          // Start ADC
        }
         // 清INT_TC中断
        SUBSRCPND_REG |= BIT_SUB_TC;
        SRCPND_REG    |= BIT_ADC;
        INTPND_REG    |= BIT_ADC;        
    }
#if 0   
    INTMSK_REG &= ~BIT_ADC;       //ADC Touch Screen Mask bit clear
    INTSUBMSK_REG &= ~(BIT_SUB_ADC | BIT_SUB_TC);
#endif
}

RAW_U32 Get_Touch_X_Pos(void)
{
	ADCTSC_REG &= ~(0x7f);

    ADCTSC_REG |= (1<<4)|(1<<0);
    ADCCON_REG |= 0x01;
    while(ADCCON_REG & 0x1);
    while(!(ADCCON_REG & 0x8000));
    xdata=(ADCDAT0_REG & 0xfff);

    ADCTSC_REG &= ~(0x7f);	
	ADCTSC_REG |= 0x53;
	return xdata;
}

RAW_U32 Get_Touch_Y_Pos(void)
{
	ADCTSC_REG &= ~(0x7f);

    ADCTSC_REG |= (1<<6)|(2<<0);
    ADCCON_REG |= 0x01;
    while(ADCCON_REG & 0x01);
    while(!(ADCCON_REG & 0x8000));
    ydata=(ADCDAT1_REG & 0xfff);    
	
	return ydata;
}





