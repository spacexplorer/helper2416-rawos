#include "GUI.h"
#include <raw_api.h>
#include "lcd_driver.h"
#include "2416_reg.h"

#define  GUI_TASK_STK_SIZE 10240
PORT_STACK gui_task_stack1[GUI_TASK_STK_SIZE];
PORT_STACK gui_task_stack2[GUI_TASK_STK_SIZE];
RAW_TASK_OBJ 		test_gui_obj[10];

void ucgui_task(void * pParam) ;
void RePaint(void *pParam);

/*XY坐标校准*/
static void Touch_Calibrate(void){
    
    GUI_TOUCH_Calibrate(0,0,480,800,240);             //X轴校验
    GUI_TOUCH_Calibrate(1,0,272,640,360);             //Y轴校验
}

static void RePaint(void *pParam)
{
	//改成按键
	GPKCON_REG  &= 0xfffffffc; //GPK0设置成输入
	GPKPU_REG   = ((GPKPU_REG & (~0x3)) | 0x2); //上拉 	 
	
	
	while(1) 
	{
		/*按键*/
		if((GPKDAT_REG & 0x1)  == 0)
		{
			Uart_Printf("GPK1_Down\n");
			GUI_SendKeyMsg(GUI_KEY_F1,1);
		}	
			
		/*触摸屏*/
		GUI_TOUCH_Exec();
		raw_sleep(1);
	}
}

void start_ucgui_test(void)
{
	Uart_Printf("ucgui_test\n");
	raw_task_create(&test_gui_obj[1], (RAW_U8 *)"ucgui_test", 0, 10, 0, gui_task_stack1, GUI_TASK_STK_SIZE, ucgui_task, 1);
	raw_task_create(&test_gui_obj[2], (RAW_U8 *)"Exec()"	, 0, 9, 0, gui_task_stack2, GUI_TASK_STK_SIZE, RePaint, 1);
}

extern void GUIDEMO_Dialog(void);
extern void DemoRedraw(void);
extern void GUIDEMO_Automotive(void);
extern void GUIDEMO_main(void);
void zgqDialogTest(void);

static void ucgui_task (void * pParam)
{
	GUI_Init();
	Display_Start(LCD_WIN_0);

	Touch_Calibrate();
	while(1)
	{
//		GUI_PID_STATE th;
		//GUIDEMO_Dialog();
		//DemoRedraw();
		//GUIDEMO_Automotive();
		GUIDEMO_main();
		//zgqDialogTest();

 
//		do{
//			GUI_TOUCH_Exec(); 
//			Uart_Printf("111111th.x=%d,th.y=%d\n",th.x,th.y);
//			raw_sleep(100);
//		}while(!GUI_TOUCH_GetState(&th)); 

//		Uart_Printf("th.x=%d,th.y=%d\n",th.x,th.y);

//		LCD_SetPixelIndex(th.x,th.y,RED_16BPP);
			
		raw_sleep(10);
	}
}





