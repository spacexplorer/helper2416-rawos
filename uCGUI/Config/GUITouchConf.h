/*
*********************************************************************************************************
*                                                uC/GUI
*                        Universal graphic software for embedded applications
*
*                       (c) Copyright 2002, Micrium Inc., Weston, FL
*                       (c) Copyright 2002, SEGGER Microcontroller Systeme GmbH
*
*              �C/GUI is protected by international copyright laws. Knowledge of the
*              source code may not be used to write a similar product. This file may
*              only be used in accordance with a license and should not be redistributed
*              in any way. We appreciate your understanding and fairness.
*
----------------------------------------------------------------------
File        : GUITouch.Conf.h
Purpose     : Configures touch screen module
----------------------------------------------------------------------
*/


#ifndef __GUITOUCH_CONF_H
#define __GUITOUCH_CONF_H

#define GUI_TOUCH_SWAP_XY       0         //X��Y����ת
#define GUI_TOUCH_MIRROR_X      0         //X������
#define GUI_TOUCH_MIRROR_Y      0         //Y������

//#define CALCULATE_AT_INT		0			//�Ƿ�������������ж���(1:�� 0:��)

//#if CALCULATE_AT_INT
//#define GUI_TOUCH_AD_LEFT       0//370      //���������AD����ֵ    
//#define GUI_TOUCH_AD_RIGHT      480//3880   //�������Ҳ�AD����ֵ    
//#define GUI_TOUCH_AD_TOP        0//3760     //����������AD����ֵ   
//#define GUI_TOUCH_AD_BOTTOM     272//320    //�������ײ�AD����ֵ  
//#else
#define GUI_TOUCH_AD_LEFT       795      	//���������AD����ֵ    
#define GUI_TOUCH_AD_RIGHT      239     	//�������Ҳ�AD����ֵ    
#define GUI_TOUCH_AD_TOP        635     	//����������AD����ֵ   
#define GUI_TOUCH_AD_BOTTOM     365     	//�������ײ�AD����ֵ  
//#endif

#endif /* GUITOUCH_CONF_H */
