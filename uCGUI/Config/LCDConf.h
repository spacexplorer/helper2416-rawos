#ifndef LCDCONF_H
#define LCDCONF_H


#define LCD_XSIZE          (480)       //水平像素  (480)
#define LCD_YSIZE          (272)       //垂直像素  (272)
#define LCD_CONTROLLER     (9320)      //控制器型号
#define LCD_BITSPERPIXEL   (16)        //总线宽度
#define LCD_FIXEDPALETTE   (565)       //色彩格式 , r=5,g=6,b=5
#define LCD_SWAP_RB        (0)         //是否红蓝交换

#endif /* LCDCONF_H */
