/*-------------------------------------------*/
/* Integer type definitions for FatFs module */
/*-------------------------------------------*/

#ifndef _INTEGER
#define _INTEGER

#include <raw_api.h>

#ifdef _WIN32	/* FatFs development platform */

#include <windows.h>
#include <tchar.h>

#else			/* Embedded platform */

/* These types must be 16-bit, 32-bit or larger integer */
typedef RAW_S32				INT;
typedef RAW_U32				UINT;


/* These types must be 8-bit integer */
typedef RAW_S8			CHAR;
typedef RAW_U8	      UCHAR;
typedef RAW_U8	BYTE;

/* These types must be 16-bit integer */
typedef RAW_S16			SHORT;
typedef RAW_U16		USHORT;
typedef RAW_U16		WORD;
typedef RAW_U16		WCHAR;

/* These types must be 32-bit integer */
typedef RAW_S32			 LONG;
typedef RAW_U32	       ULONG;
typedef RAW_U32 	    DWORD;

#endif

#endif
